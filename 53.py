fname='newfile.txt'
fo=open(fname,"w+")
print("opened a file",fname," with mode ",fo.mode)


if fo.isatty():
	print("file with decriptor ",fo.fileno,"is connected to tty device")
else:
	print("file with decriptor ",fo.fileno,"is not connected to tty device")



print("writing a string with write method")
fo.write("a sample string\n") 


print("flushed the prveioulsy written string using flush()")
fo.flush()


print("reading previously flushed (first) line without closing/reopening the file")
print("and that line is",fo.read())



print("now writing a list of lines using writelines() ")
listOfLines=["Hi TopGear,I'm line 2\n",
				"Hi TopGear,I'm line 3\n",
				"Hi TopGear,I'm line 4\n",
				"Hi TopGear,I'm line 5\n",
				"Hi TopGear,I'm line 6\n",
				"Hi TopGear,I'm line 7\n",
			]

fo.writelines(listOfLines)

fo.flush()

fo.seek(0,0) #reset the file pointer
print("after writing all the lines , now the file pointer is reset for reading pupose and tecnically the file pointer is at",fo.tell(),"position")


content=fo.read(10)
print("first 10 bytes of ",fname,"is ",content)


fo.seek(10,2) #move to 10th position from the left side of the EOF
content=fo.read()
print("last 10 bytes of ",fname,"is ",content)


fo.seek(0,0) #reset the file pointer


firstline=fo.readline()
print("first line from the file using readline()",fname,"is ",firstline)

nextLine=fo.next()
print("next line to above line from the file(used nextline())",fname,"is ",nextLine)

fo.seek(0,0) #reset the file pointer
listOfAllLines=fo.readlines()
print("all lines from the file ",fname,"in list format\n",listOfAllLines)


print("enough of file operations,so truncate this file to release your frustration using truncate()")
fo.truncate(0)

print("now close the file using close() , take rest ,bye ")
fo.close()


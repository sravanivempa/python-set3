fo=open("42.py","r")
content=fo.readlines()
fo.close()

print("actual file content\n--------------------------------------------")
for line in content:
	print(line)

print("\n")

print("reversed(in terms of lines only)file content\n--------------------------------------------")
content.reverse()
for line in content:
	print(line)

print("\n\n\n")

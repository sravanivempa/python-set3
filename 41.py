#calendar operations
import calendar
print("calendar for year 2016 with space between months as 10 characters is ",calendar.calendar(2016,c=10))
print("leap days between 1980 to 2025 are:",calendar.leapdays(1980,2025))
yr=int(input("enter a year"))
if(calendar.isleap(yr)==True):
    print("Given year is a leap year \n")
else:
    print("not a leap year \n")
m=int(input("enter month \n"))
print("calendar of month of 2016 is:",calendar.month(2016,m))

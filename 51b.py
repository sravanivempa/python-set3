def countOfTreasureInEachFile(list_of_files):
	pattern="Treasure"
	count=0
	for f in list_of_files:
		fo=open(f,"r")
		content=fo.read()
		fo.close()
		if pattern in content:
			numOfOccurences=content.count(pattern)
			print(f,"has ",numOfOccurences," times of ",pattern," in it")
		else:
			print(f,"has no",pattern)
		

countOfTreasureInEachFile(['file1.txt','file2.txt','file3.txt','file4.txt'])
print("\n"*3)

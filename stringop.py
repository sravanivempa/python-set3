def sort(list_of_numbers):
        #using buble sort 
        swap_set=True
        length=len(list_of_numbers)
        while swap_set:
                swap_set=False
                for i in range(length):
                    if i+1 != length:
                        if list_of_numbers[i] > list_of_numbers[i+1]:
                            temp=list_of_numbers[i]
                            list_of_numbers[i]=list_of_numbers[i+1]
                            list_of_numbers[i+1]=temp
                            swap_set=True
        else:
            print("sorted list is ",list_of_numbers)


def rev_str(string):
        print("reverse of the given string",string," is ",string[::-1])


def binarySearch(sorted_list,search_element):
        print("Assuming user has provided sorted_list")
        low=0
        high=len(sorted_list)-1
        mid=low+(high-low)/2
        se=search_element
        found=False
        iterate=True
        while iterate:
          if high<low:
                print("element not found")
                break
          if se < sorted_list[mid]:
                high=mid-1
                mid=low+(high-low)/2
          elif se >sorted_list[mid]:
                low=mid+1
                mid=low+(high-low)/2
          elif sorted_list[mid]==se:
                found=True
                iterate=False
        else:
          if found:
                print("element found at index",mid)


